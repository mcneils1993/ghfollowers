//
//  GFUserInfoHeaderVC.swift
//  GHFollowers
//
//  Created by Simon Mcneil on 2020-12-20.
//

import UIKit

class GFUserInfoHeaderVC: UIViewController {

	//MARK:- User HeaderView Properties
	let avatarImageView = GFAvatarImageView(frame: .zero)
	let usernameLabel = GFTitleLabel(textAlignment: .left, fontSize: 34)
	let nameLabel = GFSecondaryTitleLabel(fontSize: 18)
	let locationImageView = UIImageView()
	let locationLabel = GFSecondaryTitleLabel(fontSize: 18)
	let bioLabel = GFBodyLabel(textAlignment: .left)
	
	var user: User
	
	/* We create an instance of GFUserInfoHeaderVC, we want to pass in a user and set a user */
	init(user: User) {
		self.user = user
		super.init(nibName: nil, bundle: nil)
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
    override func viewDidLoad() {
        super.viewDidLoad()
		addSubviews()
		layoutUI()
		configureUIElements()
	}
	
	func configureUIElements() {
		avatarImageView.downloadImage(from: user.avatarUrl)
		
		usernameLabel.text = user.login
		nameLabel.text = user.name ?? "" //if user.name is nil give a blank default value
		
		locationImageView.image = SFSymbols.location
		locationImageView.tintColor = .secondaryLabel //changes the color of the SFSYMBOL
		
		locationLabel.text = user.location ?? "No Location"
		bioLabel.text = user.bio ?? "No Bio Available"
	}
	
	func addSubviews() {
		view.addSubviews(avatarImageView, usernameLabel, nameLabel, locationImageView, locationLabel, bioLabel)
	}
	
	func layoutUI() {
		let padding: CGFloat = 20 
		let textImagePadding: CGFloat = 12 //padding between the avatar image and the labels to the right of it
		locationImageView.translatesAutoresizingMaskIntoConstraints = false
		bioLabel.numberOfLines = 3
		
		NSLayoutConstraint.activate([
			avatarImageView.topAnchor.constraint(equalTo: view.topAnchor, constant: padding),
			avatarImageView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
			avatarImageView.widthAnchor.constraint(equalToConstant: 90),
			avatarImageView.heightAnchor.constraint(equalToConstant: 90),
			
			//the constraint below aligns the username label to the same top alignment of the avatarImageView
			usernameLabel.topAnchor.constraint(equalTo: avatarImageView.topAnchor),
			usernameLabel.leadingAnchor.constraint(equalTo: avatarImageView.trailingAnchor, constant: textImagePadding),
			usernameLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor),
			
			nameLabel.centerYAnchor.constraint(equalTo: avatarImageView.centerYAnchor, constant: 8),
			nameLabel.leadingAnchor.constraint(equalTo: avatarImageView.trailingAnchor, constant: textImagePadding),
			nameLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor),
			
			//the constraint below aligns the username label to the same bottom alignment of the avatarImageView
			locationImageView.bottomAnchor.constraint(equalTo: avatarImageView.bottomAnchor),
			locationImageView.leadingAnchor.constraint(equalTo: avatarImageView.trailingAnchor, constant: textImagePadding),
			locationImageView.widthAnchor.constraint(equalToConstant: 20),
			locationImageView.heightAnchor.constraint(equalToConstant: 20),
			
			locationLabel.centerYAnchor.constraint(equalTo: locationImageView.centerYAnchor),
			locationLabel.leadingAnchor.constraint(equalTo: locationImageView.trailingAnchor, constant: 5),
			locationLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor),
			
			bioLabel.topAnchor.constraint(equalTo: avatarImageView.bottomAnchor, constant: textImagePadding),
			//contraint below left aligns are label with the avatar image view left side
			bioLabel.leadingAnchor.constraint(equalTo: avatarImageView.leadingAnchor),
			bioLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor)
		])
	}
}
