//
//  GFTabBarController.swift
//  GHFollowers
//
//  Created by Simon Mcneil on 2020-12-26.
//

import UIKit

class GFTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
		UITabBar.appearance().tintColor = .systemGreen //we use the scenes tabBar and not are variable as this will make the tint app wide (every views)
		viewControllers = [createSearchNC(), createFavouritesNC()]
    }
	
	/* Think of each Navigation controller like a card. Each time we push a new view controller onto the stack. You basically put a new card on the
	   deck of cards, and that's the one you will see right away, the one on top.
	
	   When we go back, we are popping off that view controller on the top of the deck of cards to reveal that VC underneath it.
	
	   Think of the Russian doll analogy where it is just containers, holding containers.
	*/
	func createSearchNC() -> UINavigationController {
		let searchVC = SearchVC()
		searchVC.title = "Search"
		searchVC.tabBarItem = UITabBarItem(tabBarSystemItem: .search, tag: 0)
		return UINavigationController(rootViewController: searchVC)
	}
	
	func createFavouritesNC() -> UINavigationController {
		let favouritesVC = FavoritesListVC()
		favouritesVC.title = "Favourites"
		favouritesVC.tabBarItem = UITabBarItem(tabBarSystemItem: .favorites, tag: 1)
		return UINavigationController(rootViewController: favouritesVC)
	}
}
