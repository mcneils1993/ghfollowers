//
//  GFTextField.swift
//  GHFollowers
//
//  Created by Simon Mcneil on 2020-12-04.
//

import UIKit

class GFTextField: UITextField {

	override init(frame: CGRect) {
		super.init(frame: frame)
		configure()
	}
	
	//storyboard initiliazer
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	private func configure() {
		translatesAutoresizingMaskIntoConstraints = false
		
		//textField frame
		layer.cornerRadius = 10
		layer.borderWidth = 2
		layer.borderColor = UIColor.systemGray4.cgColor //when dealing with layers we need to use cgColor to change the color
		
		//textFeild Color
		textColor = .label
		tintColor = .label
		
		//Adds left side padding for the placeholder textField frame on the left side
		leftView = UIView(frame: CGRect(x: 0, y: 0, width: 14, height: 0))
		leftViewMode = .always

		font = .preferredFont(forTextStyle: .title2) //use preferred font to handle Dynamic Text
		adjustsFontSizeToFitWidth = true //this shrinks the text inside the text field
		minimumFontSize = 12 //will shrink the text to this font size minimum
		
		backgroundColor = .tertiarySystemBackground
		autocorrectionType = .no
		returnKeyType = .done //changes the keyboard return key
		clearButtonMode = .whileEditing //if we start typing in our textField an x will appear to clear all the content
		placeholder = "Enter a username"
	}
}
