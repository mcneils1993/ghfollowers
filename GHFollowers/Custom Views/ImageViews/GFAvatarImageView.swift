//
//  GFAvatarImageView.swift
//  GHFollowers
//
//  Created by Simon Mcneil on 2020-12-09.
//

import UIKit

class GFAvatarImageView: UIImageView {
	
	let cache = NetworkManager.shared.cache
	let placeholderImage = UIImage(named: "avatar-placeholder")
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		configure()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	func configure() {
		translatesAutoresizingMaskIntoConstraints = false
		layer.cornerRadius = 10
		//Need this to give the image in frame the corner radius also. Image would look like a sharp square
		clipsToBounds = true
		image = placeholderImage
	}
	
	/* Reason why we have the downloadImage here instead of network manager is because we're not really
	   handling errors here.
	*/
	func downloadImage(from urlString: String) {
		
		let cacheKey = NSString(string: urlString)
		if let image = cache.object(forKey: cacheKey) {
			self.image = UIImage(cgImage: image)
			return
		}
		
		/* Best practice is to set a default placeholder image if there is an error on getting an avatar image.
		Imagine if there are 12 images and each of them have an error. Then you'll get 12 errors.
		*/
		guard let url = URL(string: urlString) else { return }
		
		let task = URLSession.shared.dataTask(with: url) { [weak self] data, response, error in
			guard let self = self else { return }
			if error != nil { return }
			guard let response = response as? HTTPURLResponse, response.statusCode == 200 else { return }
			guard let data = data else { return }
			
			DispatchQueue.main.async {
				self.image = self.downsampleImage(imageData: data, frameSize: self.bounds.size, key: cacheKey)
			}
		}
		//don't forget calling .resume(), this is what kicks off our network call.
		task.resume()
	}
	
	func downsampleImage(imageData data: Data, frameSize: CGSize, key cacheKey: NSString) -> UIImage? {
		
		let imageSourceOptions = [kCGImageSourceShouldCache: false] as CFDictionary
		guard let imageSource = CGImageSourceCreateWithData(data as CFData, imageSourceOptions) else { return nil }
		
		//downsample the image
		let downsampleOptions = [
			kCGImageSourceCreateThumbnailFromImageAlways: true,
			kCGImageSourceCreateThumbnailWithTransform: true,
			kCGImageSourceShouldCacheImmediately: true,
			kCGImageSourceThumbnailMaxPixelSize: max(frameSize.width, frameSize.height) * UIScreen.main.scale
		] as CFDictionary
		
		guard let downsampleImage = CGImageSourceCreateThumbnailAtIndex(imageSource, 0, downsampleOptions) else { return nil }
		cache.setObject(downsampleImage, forKey: cacheKey)

		return UIImage(cgImage: downsampleImage)
	}
}
