//
//  GFButton.swift
//  GHFollowers
//
//  Created by Simon Mcneil on 2020-12-04.
//

import UIKit

class GFButton: UIButton {

	/* Are default init, if we create a button without using our custom init below then we give it a default button look by calling configure() */
	override init(frame: CGRect) {
		super.init(frame: frame)
		configure()
	}
	
	/* This one gets called when we init the GF button via storyboard. Since we aren't using storyboard we just give it a message. */
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	//Create a custom init so we can  bass in different background colors and titls for are button	
	init(backgroundColor: UIColor, title: String) {
		super.init(frame: .zero) // we give it frame of zero because we will set the width and height progromatically
		self.backgroundColor = backgroundColor
		self.setTitle(title, for: .normal)
		configure()
	}
	
	private func configure() {
		translatesAutoresizingMaskIntoConstraints = false //this says to use auto layout. we need this if we want to create view progrmatically
		layer.cornerRadius = 10
		titleLabel?.textColor = .white
		titleLabel?.font = .preferredFont(forTextStyle: .headline)
	}
	
	func set(backgroundColor: UIColor, title: String) {
		self.backgroundColor = backgroundColor
		setTitle(title, for: .normal)
	}
}
