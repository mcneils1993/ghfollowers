//
//  FollowerCell.swift
//  GHFollowers
//
//  Created by Simon Mcneil on 2020-12-09.
//

import UIKit

class FollowerCell: UICollectionViewCell {
	
	//frame is .zero because we will be initializing size via our constraints
	let avatarImageView = GFAvatarImageView(frame: .zero)
	let usernameLabel = GFTitleLabel(textAlignment: .center, fontSize: 16)

	override init(frame: CGRect) {
		super.init(frame: frame)
		configure()
	}
		
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	
	func set(follower: Follower) {
		usernameLabel.text = follower.login
		avatarImageView.downloadImage(from: follower.avatarUrl)
	}
	
	private func configure() {
		addSubviews(avatarImageView, usernameLabel)

		let padding: CGFloat = 8		
		NSLayoutConstraint.activate([
			avatarImageView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: padding),
			avatarImageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: padding),
			avatarImageView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -padding),
			avatarImageView.heightAnchor.constraint(equalTo: avatarImageView.widthAnchor)
		])
		
		NSLayoutConstraint.activate([
			usernameLabel.topAnchor.constraint(equalTo: avatarImageView.bottomAnchor, constant: 12),
			usernameLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: padding),
			usernameLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -padding),
		])
	}
}
