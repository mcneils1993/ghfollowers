//
//  GFTitleLabel.swift
//  GHFollowers
//
//  Created by Simon Mcneil on 2020-12-05.
//

import UIKit

class GFTitleLabel: UILabel {

	override init(frame: CGRect) {
		super.init(frame: frame)
		configure()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	init(textAlignment: NSTextAlignment, fontSize: CGFloat) {
		super.init(frame: .zero)
		self.textAlignment = textAlignment
		self.font = .systemFont(ofSize: fontSize, weight: .bold)
		configure()
	}
	
	private func configure() {
		translatesAutoresizingMaskIntoConstraints = false
		textColor = .label
		adjustsFontSizeToFitWidth = true //shrinks the text a little bit to adjust for longer texts
		minimumScaleFactor = 0.9 //goes down to 90 percent for are adjustFontSize
		lineBreakMode = .byTruncatingTail //this will give the effect Simo.... if the text is to long and goes behind our minimumScaleFactor
	}
}
