//
//  GFSecondaryTitleLabel.swift
//  GHFollowers
//
//  Created by Simon Mcneil on 2020-12-20.
//

import UIKit

class GFSecondaryTitleLabel: UILabel {

	override init(frame: CGRect) {
		super.init(frame: frame)
		configure()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	init(fontSize: CGFloat) {
		super.init(frame: .zero)
		font = .systemFont(ofSize: fontSize, weight: .medium)
		configure()
	}
	
	private func configure() {
		translatesAutoresizingMaskIntoConstraints = false
		textColor = .secondaryLabel
		adjustsFontSizeToFitWidth = true //shrinks the text a little bit to adjust for longer texts
		minimumScaleFactor = 0.90 //goes down to 75 percent for are adjustsFontSizeToFitWidth
		lineBreakMode = .byTruncatingTail //this will give the effect Simo.... if the text is to long and goes behind our minimumScaleFactor
	}
}
