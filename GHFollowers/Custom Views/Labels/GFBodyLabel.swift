//
//  GFBodyLabel.swift
//  GHFollowers
//
//  Created by Simon Mcneil on 2020-12-05.
//

import UIKit

class GFBodyLabel: UILabel {
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		configure()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	init(textAlignment: NSTextAlignment) {
		super.init(frame: .zero)
		self.textAlignment = textAlignment
		configure()
	}
	
	private func configure() {
		translatesAutoresizingMaskIntoConstraints = false
		font = .preferredFont(forTextStyle: .body)
		adjustsFontForContentSizeCategory = true //will change the dynamic type as we move the dynamic type slider in xCode, good for debugging
		textColor = .secondaryLabel
		adjustsFontSizeToFitWidth = true //shrinks the text a little bit to adjust for longer texts
		minimumScaleFactor = 0.75 //goes down to 75 percent for are adjustFontSize
		lineBreakMode = .byWordWrapping
	}
}
