import UIKit

/* The purpose for Scene Delegate is essentially to allow multi scene support such as iPad for MultiTask. Multi scenes side by side */
class SceneDelegate: UIResponder, UIWindowSceneDelegate {

	var window: UIWindow?

	func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
		
		guard let windowScene = (scene as? UIWindowScene) else { return }
			
		//4 lines needed to present your first view controller
		window = UIWindow(frame: windowScene.coordinateSpace.bounds)
		window?.windowScene = windowScene
		window?.rootViewController = GFTabBarController()
		window?.makeKeyAndVisible()
		
		configureNavigationBar()
	}
	
	func configureNavigationBar() {
		UINavigationBar.appearance().tintColor = .systemGreen //makes all of our navigation controllers have this.
	}

	func sceneDidDisconnect(_ scene: UIScene) {
		
	}

	func sceneDidBecomeActive(_ scene: UIScene) {
		
	}

	func sceneWillResignActive(_ scene: UIScene) {
		
	}

	func sceneWillEnterForeground(_ scene: UIScene) {
		
	}

	func sceneDidEnterBackground(_ scene: UIScene) {
		
	}
}

