//
//  UIView+Ext.swift
//  GHFollowers
//
//  Created by Simon Mcneil on 2021-01-04.

import UIKit

extension UIView {
	
	//variadic parameter, this means I can add in any number of views into addSubviews. It will automatically make are parameter views of type array
	func addSubviews(_ views: UIView...) {
		for view in views {
			/* view.addSubview is equivalent to grabbing that image view from are storyboard library and then dragging and dropping it onto our view controller */
			addSubview(view)
		}
	}
	
	func pindToEdges(of superView: UIView) {
		translatesAutoresizingMaskIntoConstraints = false
		NSLayoutConstraint.activate([
			topAnchor.constraint(equalTo: superView.topAnchor),
			leadingAnchor.constraint(equalTo: superView.leadingAnchor),
			trailingAnchor.constraint(equalTo: superView.trailingAnchor),
			bottomAnchor.constraint(equalTo: superView.bottomAnchor)
		])
	}
}
