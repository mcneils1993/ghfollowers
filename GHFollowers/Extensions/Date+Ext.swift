//
//  Date+Ext.swift
//  GHFollowers
//
//  Created by Simon Mcneil on 2020-12-20.
//

import Foundation

extension Date {
	
	func convertToMonthYearFormat() -> String {
		let dateFormmater = DateFormatter()
		dateFormmater.dateFormat = "MMMM yyyy"
		return dateFormmater.string(from: self)
	}
}
