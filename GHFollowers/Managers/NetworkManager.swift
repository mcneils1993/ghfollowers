//
//  NetworkManager.swift
//  GHFollowers
//
//  Created by Simon Mcneil on 2020-12-07.
//

import UIKit

class NetworkManager {
	static let shared = NetworkManager()
	private let baseURL = "https://api.github.com/users/"
	private init() {}
	
	//Are image cache
	var cache = NSCache<NSString, CGImage>()
	
	func getFollowers(for username: String, page: Int, completion: @escaping (Result<[Follower], GFError>) -> ()) {
		let endpoint = baseURL + "\(username)/followers?per_page=100&page=\(page)"
		
		guard let url = URL(string: endpoint) else {
			completion(.failure(.invalidUsername))
			return
		}
		
		let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
			if let _ = error {
				completion(.failure(.unableToComeplete))
				return
			}
			
			guard let response = response as? HTTPURLResponse, response.statusCode == 200 else {
				completion(.failure(.invalidResponse))
				return
			}
			
			guard let data = data else {
				completion(.failure(.invalidData))
				return
			}
			
			//parsing
			do {
				let decoder = JSONDecoder()
				decoder.keyDecodingStrategy = .convertFromSnakeCase
				let followers = try decoder.decode([Follower].self, from: data)
				completion(.success(followers))
			} catch {
				completion(.failure(.invalidData))
			}
		}
		task.resume() //this is what starts the network call
	}
	
	/* What @escaping is:
		- Closue are either escpaing or nonescaping. An escaping closure can outlive this getUserInfo function.
	      What is means is that they need to live longer than this function. So this function will be ran, it'll throw it
	      on a background thread, and then this function will be done and out, but the closure might not be yet.
	
		- Function calls usually live on the stack. When the function has finished all its variables are removed from the stack.
	      However if I want a parameter to outlive the function then it needs to be in the heap where the reference variables are.
		  An example of this is when we pass variables as &inout, that would have a reference to the heap.
	*/
	func getUserInfo(for username: String, completion: @escaping (Result<User, GFError>) -> ()) {
		let endpoint = baseURL + "\(username)"
		
		guard let url = URL(string: endpoint) else {
			completion(.failure(.invalidUsername))
			return
		}
		
		let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
			if let _ = error {
				completion(.failure(.unableToComeplete))
				return
			}
			
			guard let response = response as? HTTPURLResponse, response.statusCode == 200 else {
				completion(.failure(.invalidResponse))
				return
			}
			
			guard let data = data else {
				completion(.failure(.invalidData))
				return
			}
			
			//parsing
			do {
				let decoder = JSONDecoder()
				decoder.keyDecodingStrategy = .convertFromSnakeCase
				decoder.dateDecodingStrategy    = .iso8601
				let user = try decoder.decode(User.self, from: data)
				completion(.success(user))
			} catch {
				completion(.failure(.invalidData))
			}
		}
		task.resume() //this is what starts the network call
	}
}
