
import UIKit

//Should probably change this to a singleton...
enum UIHelper {
	/* A flow layout basically determines how our collectionView looks. With this application we are
	implementing a three column look. */
	static func createThreeColumnFlowLayout(in view: UIView) -> UICollectionViewFlowLayout {
		let width = view.bounds.width
		let padding: CGFloat = 12
		
		/* minimum spacing is the spacing to put IN BETWEEN each cell.
		So in between the left and the middle cell , and between the middle and the right cell */
		let minimumItemSpacing: CGFloat = 10
		
		/* The reason we have padding * 2 it's to account for the right and left of the screen for our collectionView cells. Then we're saying * 2 for the minimumItemSpacing because we harve
		TWO cells on both sides of the middle cell. This would change if we want more cells in a row.
		*/
		let availableWidth = width - (padding * 2) - (minimumItemSpacing * 2)
		
		let itemWidth = availableWidth / 3
		
		let flowLayout = UICollectionViewFlowLayout()
		flowLayout.sectionInset = UIEdgeInsets(top: padding, left: padding, bottom: padding, right: padding)
		flowLayout.itemSize = CGSize(width: itemWidth, height: itemWidth + 40)
		
		return flowLayout
	}
}
