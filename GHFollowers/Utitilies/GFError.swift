//
//  ErrorMessage.swift
//  GHFollowers
//
//  Created by Simon Mcneil on 2020-12-08.
//

import Foundation

/* RawValue: All the cases conform to one type, can also let us conform to Error (for result type)

   Associated Value: Each case can have a different type. EX:
		- enum Barcode {
			case upc(Int, Int, Int)
			case qrCode(String)
		}
*/
enum GFError: String, Error {
	case invalidUsername = "This username created an invalid request. Please try again."
	case unableToComeplete = "Unable to complete request. Please check internet connection."
	case invalidResponse = "Invalid response from the server. Please try again."
	case invalidData = "The data recieved from the server was invalid. Please try again."
	case unableToFavorite = "There was an error favoriting this user"
	case alreadyInFavorites = "User already saved"
}
