//
//  Follower.swift
//  GHFollowers
//
//  Created by Simon Mcneil on 2020-12-06.
//

import Foundation

/* Made this conform to Hashable so we can use it in our UICollectionViewDiffableDataSource */
struct Follower: Codable, Hashable {
	var login: String
	var avatarUrl: String
}
