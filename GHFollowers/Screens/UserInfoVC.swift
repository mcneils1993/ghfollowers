//
//  UserInfoVC.swift
//  GHFollowers
//
//  Created by Simon Mcneil on 2020-12-19.
//

import UIKit

/* We place are DelegateProtocol where we are sending the action. In this case we tap a user follower and the action is being sent to here.  */
protocol UserInfoVCDelegate: class {
	func didRequestFollowers(for username: String)
}

class UserInfoVC: UIViewController {

	//Are scrollView views
	let scrollView = UIScrollView()
	let contentView = UIView()
	
	let headerView = UIView()
	let itemViewOne = UIView()
	let itemViewTwo = UIView()
	let dateLabel = GFBodyLabel(textAlignment: .center)
	var itemViews: [UIView] = []
	
	var username: String!
	weak var delegate: UserInfoVCDelegate?
	
    override func viewDidLoad() {
        super.viewDidLoad()
		configureViewController()
		configureScrollView()
		layoutUI()
		getUserInfo()

    }
	
	func configureViewController() {
		view.backgroundColor = .systemBackground
		let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(dismissVC))
		navigationItem.rightBarButtonItem = doneButton
	}
	
	func configureScrollView() {
		view.addSubview(scrollView)
		scrollView.addSubview(contentView)
		
		scrollView.pindToEdges(of: view)
		contentView.pindToEdges(of: scrollView)
		
		NSLayoutConstraint.activate([
			contentView.widthAnchor.constraint(equalTo: scrollView.widthAnchor),
			contentView.heightAnchor.constraint(equalToConstant: 600)
		])
	}
	
	func layoutUI() {
		let padding: CGFloat = 20
		let itemHeight: CGFloat = 140
		itemViews = [headerView, itemViewOne, itemViewTwo, dateLabel]

		for itemView in itemViews {
			//remember the contentView is the view inside the scrollView
			contentView.addSubview(itemView)
			itemView.translatesAutoresizingMaskIntoConstraints = false
			
			NSLayoutConstraint.activate([
				itemView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: padding),
				itemView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -padding),
			])
		}
				
		NSLayoutConstraint.activate([
			headerView.topAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.topAnchor),
			headerView.heightAnchor.constraint(equalToConstant: 180),
			
			itemViewOne.topAnchor.constraint(equalTo: headerView.bottomAnchor, constant: padding),
			itemViewOne.heightAnchor.constraint(equalToConstant: itemHeight),
			
			itemViewTwo.topAnchor.constraint(equalTo: itemViewOne.bottomAnchor, constant: padding),
			itemViewTwo.heightAnchor.constraint(equalToConstant: itemHeight),
			
			dateLabel.topAnchor.constraint(equalTo: itemViewTwo.bottomAnchor, constant: padding),
		])
	}
	
	func getUserInfo() {
		NetworkManager.shared.getUserInfo(for: username) { [weak self] result in
			guard let self = self else { return }
			
			switch result {
				case .success(let user):
					DispatchQueue.main.async {
						self.configureUIElements(with: user)
					}
				case .failure(let error):
					self.presentGFAlertOnMainThread(title: "Something Went Wrong", message: error.rawValue, buttonTitle: "Ok")
			}
		}
	}
	
	
	func configureUIElements(with user: User) {
		let repoItemVC = GFRepoItemVC(user: user, delegate: self) 
		let followerItemVC = GFFollowerItemVC(user: user, delegate: self)
			
		self.add(childVC: GFUserInfoHeaderVC(user: user), to: self.headerView)
		self.add(childVC: repoItemVC, to: self.itemViewOne)
		self.add(childVC: followerItemVC, to: self.itemViewTwo)
		self.dateLabel.text = "Joined \(user.createdAt.convertToMonthYearFormat())"
	}
	
	func add(childVC: UIViewController, to containerView: UIView) {
		//This method is only intended to be called by an implementation of a custom container view controller
		addChild(childVC)
		
		containerView.addSubview(childVC.view)
		childVC.view.frame = containerView.bounds //sets the size of the VC to match our containerView
		//childVC.didMove(toParent: self)
	}
 
	@objc func dismissVC() {
		dismiss(animated: true)
	}
}

extension UserInfoVC: GFRepoItemVCDelegate {
	func didTapGitHubProfile(for user: User) {
		//Show safari view controller
		guard let url = URL(string: user.htmlUrl) else {
			presentGFAlertOnMainThread(title: "Invalid URL", message: "The URL attached to this user is invalid.", buttonTitle: "Ok")
			return
		}
		presentSafariVC(with: url)
	}
}

extension UserInfoVC: GFFollowerItemVCDelegate {
	func didTapGetFollowers(for user: User) {
		guard user.followers != 0 else {
			presentGFAlertOnMainThread(title: "No Followers", message: "User has no followers ☹️", buttonTitle: "So Sad")
			return
		}
		delegate?.didRequestFollowers(for: user.login)
		dismissVC()
	}
}
