//
//  SearchVC.swift
//  GHFollowers
//
//  Created by Simon Mcneil on 2020-12-02.
//

import UIKit

class SearchVC: UIViewController {
	
	let logoImageView = UIImageView()
	let userNameTextField = GFTextField()
	let gitFollowersButton = GFButton(backgroundColor: .systemGreen, title: "Git Followers")
	var logoImageViewTopConstraint: NSLayoutConstraint!
	
	var isUserNameEntered: Bool {
		return !userNameTextField.text!.isEmpty
	}

    override func viewDidLoad() {
        super.viewDidLoad()
		view.backgroundColor = .systemBackground
		view.addSubviews(logoImageView, userNameTextField, gitFollowersButton)
		configureLogoImageView()
		configureTextFeild()
		configureGitFollowersButton()
		
		createDismissKeyBoardTapGesture()
    }
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		userNameTextField.text = ""
		/* We do this in viewWillAppear because this gets called everytime we come back to this view. If we did this in viewDidLoad we would only hide the
		   nav bar the first time we load on this viewController.
		
		   Reason why we use setNavigtionBarHidden(true, animated: true) is because in the next view controllers we navigate to we show the show navigation bar, and if we used
		   .isNavigationBarHidden then the animation to display the nav bar isn't as clean. 
		*/
		navigationController?.setNavigationBarHidden(true, animated: true)
	}
	
	//Function to handle dismissing the keyboard when tapping on the screen outside of the keyboard
	func createDismissKeyBoardTapGesture() {
		let tap = UITapGestureRecognizer(target: view, action: #selector(UIView.endEditing(_:)))
		view.addGestureRecognizer(tap)
	}
	
	@objc func pushFollowersListVC() {
		guard isUserNameEntered else {
			presentGFAlertOnMainThread(title: "Empty Username", message: "Please Enter a username. We need to know who to look for 😄", buttonTitle: "Ok")
			return
		}
		
		let followerListVC = FollowerListVC(username: userNameTextField.text!)
		userNameTextField.resignFirstResponder() //gets rid of the keyboard
		navigationController?.pushViewController(followerListVC, animated: true)
	}
	
	func configureLogoImageView() {
		logoImageView.translatesAutoresizingMaskIntoConstraints = false
		logoImageView.image = Images.ghLogo
		
		let topConstraintConstrant: CGFloat = DeviceTypes.isiPhoneSE || DeviceTypes.isiPhone8Zoomed ? 20: 80
		logoImageViewTopConstraint = logoImageView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: topConstraintConstrant)
		logoImageViewTopConstraint.isActive = true
		
		NSLayoutConstraint.activate([
			logoImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
			logoImageView.heightAnchor.constraint(equalToConstant: 200),
			logoImageView.widthAnchor.constraint(equalToConstant: 200)
		])
	}
	
	func configureTextFeild() {
		userNameTextField.delegate = self
		NSLayoutConstraint.activate([
			userNameTextField.topAnchor.constraint(equalTo: logoImageView.bottomAnchor, constant: 48),
			userNameTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 50),
			userNameTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -50), //remember trailing constant is usually negative value of leading constant
			userNameTextField.heightAnchor.constraint(equalToConstant: 50)
		])
	}
	
	func configureGitFollowersButton() {
		gitFollowersButton.addTarget(self, action: #selector(pushFollowersListVC), for: .touchUpInside)
		NSLayoutConstraint.activate([
			gitFollowersButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -50), //trailing on bottom have to use negative numbers
			gitFollowersButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 50),
			gitFollowersButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -50),
			gitFollowersButton.heightAnchor.constraint(equalToConstant: 50)
		])
	}
}


extension SearchVC: UITextFieldDelegate {
	//executes when we tap the return button
	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		pushFollowersListVC()
		return true
	}
}
