//
//  FavouriteListVC.swift
//  GHFollowers
//
//  Created by Simon Mcneil on 2020-12-02.
//

import UIKit

class FavoritesListVC: GFDataLoadingVC {

	let tableView = UITableView()
	var favorites: [Follower] = []
	
    override func viewDidLoad() {
        super.viewDidLoad()
		configureViewController()
		configureTableView()
		tableView.tableFooterView = UIView(frame: .zero)
	}
	
	/*Remember viewDidload only gets called once, so we need to call getFavorites in viewDidAppear */
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(true)
		getFavorites()
	}
	
	private func configureViewController() {
		navigationController?.navigationBar.prefersLargeTitles = true
		title = "Favorites"
		view.backgroundColor = .systemBackground
	}
	
	private func configureTableView() {
		view.addSubview(tableView)
		tableView.delegate = self
		tableView.dataSource = self
		
		//remember view.bounds says fill up the whole view with the view so in this case the tableView
		tableView.frame = view.bounds
		tableView.rowHeight = 80
		tableView.register(FavoriteCell.self, forCellReuseIdentifier: String(describing: FavoriteCell.self))
	}
	
	private func getFavorites() {
		PersistenceManager.retrieveFavorites { [weak self] result in
			guard let self = self else { return }
			switch result {
				case .success(let favorites):
					if favorites.isEmpty {
						self.showEmptyStateView(with: "No Favorites?\nAdd one on the followr screen", in: self.view)
					} else {
						self.favorites = favorites
						DispatchQueue.main.async {
							self.tableView.reloadData()
							/* Handles the edge case that if we launch this view first and no favorites, leave
							   add a favorite and then comeback the showEmptyStateView would still display on top.
							
							   So we need to add the tableView view to the front.
							*/
							self.view.bringSubviewToFront(self.tableView)
						}
					}
				case .failure(let error):
					self.presentGFAlertOnMainThread(title: "Something went wrong", message: error.rawValue, buttonTitle: "Ok")
			}
		}
	}
}

//We keep our data source in the same file because it's not complex, it's pretty straight forward.
extension FavoritesListVC: UITableViewDataSource, UITableViewDelegate {
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return favorites.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: FavoriteCell.self)) as! FavoriteCell
		cell.set(favorite: favorites[indexPath.row])
		return cell
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		let favorite = favorites[indexPath.row]
		let destVC = FollowerListVC(username: favorite.login)
		navigationController?.pushViewController(destVC, animated: true)
	}
	
	func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
		//if we're not doing a .delete then don't do anything else
		guard editingStyle == .delete else { return }
		
		PersistenceManager.updateWith(favorite: favorites[indexPath.row], actionType: .remove) { [weak self] error in
			guard let self = self else { return }
			
			/* This read as if I have an error continue, but if I don't have an error then go inside the condition block */
			guard let hasError = error else {
				self.favorites.remove(at: indexPath.row)
				tableView.deleteRows(at: [indexPath], with: .left)
				return
			}
			self.presentGFAlertOnMainThread(title: "Unable to Remove", message: hasError.rawValue, buttonTitle: "Ok")
		}
	}
}
