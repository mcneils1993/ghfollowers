//
//  FollowerListVC.swift
//  GHFollowers
//
//  Created by Simon Mcneil on 2020-12-05.
//

import UIKit

protocol FollowerListVCDelegate: class {
	func didRequestFollowers(for username: String)
}

class FollowerListVC: GFDataLoadingVC {
	
	/* Enums are hashable by default */
	enum Section {
		case main
	}
	
	var username: String!
	var followers: [Follower] = []
	var filteredFollowers: [Follower] = []
	var page = 1
	var hasMoreFollowers = true
	var isSearching = false
	var isLoadingMoreFollowers = false
	
	var collectionView: UICollectionView!
	var dataSource: UICollectionViewDiffableDataSource<Section, Follower>!
	
	init(username: String) {
		super.init(nibName: nil, bundle: nil)
		self.username = username
		title = username
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		configureViewController()
		configureSearchController()
		configureCollectionView()
		getFollowers(username: username, page: page)
		configureDataSource()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(true)
		
		/* Reason why we use setNavigtionBarHidden(false, animated: true) is because in the view controller we navigate from we dont show the navigation bar, and if we used
		.isNavigationBarHidden = true here then the nav bar displays before this view controller is loaded giving it a "snappy" nav bar display.
		
		navigationController?.setNavigationBarHidden(false, animated: true) makes presenting the Navigation Bar much cleaner when landing on our VC.
		*/
		navigationController?.setNavigationBarHidden(false, animated: true)
	}
	
	func configureViewController() {
		navigationController?.navigationBar.prefersLargeTitles = true
		view.backgroundColor = .systemBackground
		let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addButtonTapped))
		navigationItem.rightBarButtonItem = addButton
	}
	
	func configureCollectionView() {
		/* view.bounds will set our view to the whole size of the phone screen.
		Therefore there's no needs for contstraints when creating our collectionView. */
		collectionView = UICollectionView(frame: view.bounds, collectionViewLayout: UIHelper.createThreeColumnFlowLayout(in: view))
		view.addSubview(collectionView)
		collectionView.delegate = self
		collectionView.backgroundColor = .systemBackground
		collectionView.register(FollowerCell.self, forCellWithReuseIdentifier: String(describing: FollowerCell.self))
	}
	
	func configureSearchController() {
		let searchController = UISearchController()
		searchController.searchResultsUpdater = self
		searchController.searchBar.placeholder = "Search Follower"
		searchController.obscuresBackgroundDuringPresentation = false
		
		//adds are search controller to the navigation bar
		navigationItem.searchController = searchController
		
	}
	
	func getFollowers(username: String, page: Int) {
		showLoadingView()
		isLoadingMoreFollowers = true
		
		NetworkManager.shared.getFollowers(for: username, page: page) { [weak self] result in
			//just unwrapping the optional self
			guard let self = self else { return }
			self.dismissLoadingView()
			
			switch result {
				case .success(let followers):
					/* Update are flag to not fetch more users when we scroll down to the botom of the scrollView */
					if followers.count < 100 { self.hasMoreFollowers = false }
					
					self.followers.append(contentsOf: followers)
					if self.followers.isEmpty {
						let message = "This user doesn't have any followers, 😆"
						DispatchQueue.main.async {
							self.showEmptyStateView(with: message, in: self.view)
						}
						return
					}
					self.updateData(on: self.followers)
					
				case .failure(let error):
					self.presentGFAlertOnMainThread(title: "Bad Stuff Happened", message: error.rawValue, buttonTitle: "Ok")
			}
			
			self.isLoadingMoreFollowers = false
		}
	}
	
	func configureDataSource() {
		dataSource = UICollectionViewDiffableDataSource<Section, Follower>(collectionView: collectionView, cellProvider: { (collectionView, indexPath, follower) -> UICollectionViewCell? in
			let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: FollowerCell.self), for: indexPath) as! FollowerCell
			
			/* What's happening here is that every follower will be set to a follower cell, and then through the
			.set function inside FollowerCell will set the text label of the cell and the avatar image.
			*/
			cell.set(follower: follower)
			return cell
		})
	}
	
	/* Everytime we want to take a snapShot of the collectionView then we will call updateData */
	func updateData(on followers: [Follower]) {
		var snapshot = NSDiffableDataSourceSnapshot<Section, Follower>()
		snapshot.appendSections([.main])
		snapshot.appendItems(followers)
		
		DispatchQueue.main.async {
			self.dataSource.apply(snapshot, animatingDifferences: true)
		}
	}
	
	@objc func addButtonTapped() {
		showLoadingView()
		NetworkManager.shared.getUserInfo(for: username) { [weak self] result in
			guard let self = self else { return }
			self.dismissLoadingView()
			
			switch result {
				case .success(let user):
					let favorite = Follower(login: user.login, avatarUrl: user.avatarUrl)
					
					PersistenceManager.updateWith(favorite: favorite, actionType: .add) { [weak self] error in
						guard let self = self else { return }
						
						//Kind of backwards but if the error is nil that means are save was good.
						guard let error = error else {
							self.presentGFAlertOnMainThread(title: "Success!", message: "You have successfully saved this user 😀", buttonTitle: "Hooray!")
							return
						}
						
						self.presentGFAlertOnMainThread(title: "Something went wrong", message: error.rawValue, buttonTitle: "Ok")
					}
					
				case .failure(let error):
					self.presentGFAlertOnMainThread(title: "Something Went Wrong", message: error.rawValue, buttonTitle: "Ok")
			}
		}
	}
	
	
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		let activeArray = isSearching ? filteredFollowers : followers
		let follower = activeArray[indexPath.item]
		
		let destVC = UserInfoVC()
		destVC.username = follower.login
		//This is saying our FollowerListVC is listening to the UserInfoVC meaning our communication pathway is set
		destVC.delegate = self
		
		//we add destVC to UINavigationController because we want to add a Navigation for are modal
		let navController = UINavigationController(rootViewController: destVC)
		//present will make the view appear modally, as opposed to using navigationController?.pushViewController
		present(navController, animated: true)
	}
}

/* UICollectionViewDelegate also gives us the scrollViewDelegate. */
extension FollowerListVC: UICollectionViewDelegate {
	
	func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
		//this how far we've scrolled down
		let offsetY = scrollView.contentOffset.y //if this was horizontal you'd use .x
		
		//entire height of the scrollView due to all of the content.
		let contentHeight = scrollView.contentSize.height
		
		//height of the screen
		let height = scrollView.frame.size.height
	
		//calculating when we're at the bottom of the screen. If case is true then we call next set of followers.
		if offsetY > contentHeight - height {
			//if no more followers to fetch then exit
			guard hasMoreFollowers, !isLoadingMoreFollowers else { return }
			page += 1
			getFollowers(username: username, page: page)
		}
	}
}

extension FollowerListVC: UISearchResultsUpdating {
	/* What this is doing is letting the VC know that everytime I update the search results in the search bar
	It is letting it know that something changed.
	*/
	func updateSearchResults(for searchController: UISearchController) {
		guard let searchText = searchController.searchBar.text, !searchText.isEmpty else {
			filteredFollowers.removeAll()
			updateData(on: followers)
			isSearching = false
			return
		}
		
		isSearching = true
		filteredFollowers = followers.filter { $0.login.lowercased().contains(searchText.lowercased()) }
		updateData(on: filteredFollowers)
	}
}

extension FollowerListVC: UserInfoVCDelegate {
	func didRequestFollowers(for username: String) {
		//get followers for sent user from UserInfoVC view after tapping on Get Followers button.
		self.username = username
		title = username
		page = 1
		followers.removeAll()
		filteredFollowers.removeAll()
		collectionView.scrollToItem(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
		hasMoreFollowers = true
		getFollowers(username: username, page: page)
	}
}
